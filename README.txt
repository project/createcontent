


        createcontent
        Provided by http://webschuur.com
        Developed by Bèr Kessels



        -------------------------------------------------------------------------------
        INSTALLATION
        -------------------------------------------------------------------------------

        todo

        -------------------------------------------------------------------------------
        PERMISSIONS
        -------------------------------------------------------------------------------

        permission
          - description


        -------------------------------------------------------------------------------
        PUBLIC API
        -------------------------------------------------------------------------------

        Module implements a new hoo that allows you to override the createcontent page
        from your module.

        /**
         * Implementation of hook_createcontent()
         * @param array $show an array of menu-items used in theme_node_add_list() and
         *              provided by menu_get_item('node/add').
         *              We added a createcontent_type key to this menu-item-array, so that
         *              you can match against content types in your hook implementation.
         * @return array the modified array.
         */
        hook_createcontent($show) {

        }



        -------------------------------------------------------------------------------
        CONVENTIONS
        -------------------------------------------------------------------------------

        todo



